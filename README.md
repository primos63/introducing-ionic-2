# Awesome iTunes Browser

This is the companion code for the course ["Introducing Ionic2"](https://www.packtpub.com/web-development/introducing-ionic-2-video) by Mathieu Chauvinc, published by [Packt](http://packtpub.com)

## Latest branch

Please make sure to use the branch **beta2** to get the code corresponding to the latest update using Ionic 2 beta 2.

## Change Log
### 2016-07-23 
1. changes done to full app not by video section if you check out a different branch then you won't be able to use the latest Ionic version
2. typings were installed globally sudo npm install -g typings
3.    run npm install
4.    run ionic platforms add <android | ios | browser>
5.    run ionic serve or ionic emulate <android | ios>
6.    Movies don't play in the browser or on android because of the video format
 
## Original Changes
1. Throughout the course, TypeScript will be preferred over plain Javascript. See next point.
1. Section 1 video 2: Please make sure to add the `--ts` flag to the `ionic start` command in order to get a TypeScript project.  It is highly advisable to do so in order to follow the course more easily.
1. Section 1 video 3: As of beta1, SCSS files are found under the "app/theme" folder
1. Section 7 video 1: As of beta1, SCSS files are found under the "app/theme" folder


## How to use

### Getting the code for a specific video

Simply use git checkout with the [appropriate tag](#markdown-header-how-to-use).
```sh
$ git checkout s3v1
```
Note that you will then be in a detached HEAD state so you might want to
```sh
$ git checkout -b a_new_branch
```
in order to keep your own code changes.

### How do tags work?

Each section and each video have 2 tags corresponding to their start and end.

Sections follow the pattern "s<section number>_start" or "s<section number>_start". For example `s3_start` is the code as it is at the beginning of section 3; and the code at the end of section 5 would be found with the tag `s5_end`.

Videos follow the pattern "s<section number>v<video number>" for start and "after_s<section number>v<video number>" for end. For example `s4v2` is the code at the beginning of the 2nd video of section 4. And the code after the first video of section would be at tag `after_s2v1`